mod board;
mod piece;
mod game;

use std::{iter::empty, ops::{Index, IndexMut}, char::from_digit};

use piece::*;

use board::*;

use crate::game::Game;


pub fn run() {
    println!("hi from lib.rs");
    let board = Board::new();
    let mut game = Game {board: board, en_passant_square: Some((0, 0).into()), white_can_castle: true, black_can_castle: true, fifty_move_rule_count: 0};
    let board = &mut game.board;
    f(board, 2, 2);
    f(board, 2, 5);
    f(board, 4, 2);
    board.at_xy_mut(4, 2).unwrap().as_mut().unwrap().color = PieceColor::White;
    let s22 = board.at_xy(2, 2);
    println!("{:?}", s22);
    let moves = s22.expect("e").unwrap().valid_moves((2, 2).into(), &game);
    println!("{:?}", moves);
    let ps: String = Position(2, 2).into();
    println!("{}", ps);
    
}

fn f(board: &mut Board, x: usize, y: usize) {
    let s1 = board.at_xy_mut(x, y).expect("e");
    *s1 = Some(Piece {
        kind: PieceKind::Pawn,
        color: PieceColor::Black,
    })
}
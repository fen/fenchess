use crate::board::*;

pub struct Game {
    pub board: Board,
    pub en_passant_square: Option<Position>,
    pub white_can_castle: bool,
    pub black_can_castle: bool,
    pub fifty_move_rule_count: u8,
}
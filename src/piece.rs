use crate::board::*;
use crate::game::*;

#[derive(Clone, Copy, PartialEq, Debug)]
pub enum PieceColor {
    White,
    Black,
}

#[derive(Clone, Copy, Debug)]
pub enum PieceKind {
    Pawn,
    Knight,
    Bishop,
    Rook,
    Queen,
    King,
}

#[derive(Clone, Copy, Debug)]
pub struct Piece {
    pub kind: PieceKind,
    pub color: PieceColor,
}

impl Piece {
    pub fn is_valid(&self, mv: ChessMove, game: &Game) -> bool {
        true
    }

    pub fn attacks(&self, mv: ChessMove, game: &Game) -> bool {
        true
    }

    pub fn valid_moves(&self, pos: Position, game: &Game) -> Vec<Position> {
        self.iter_displace(game, pos, 1, 0)
    }

    fn iter_displace(
        &self,
        game: &Game,
        start_pos: Position,
        dx: usize,
        dy: usize,
    ) -> Vec<Position> {
        let mut possible_moves: Vec<Position> = Vec::with_capacity(4);
        let mut current_check_pos: Position = (start_pos.0 + dx, start_pos.1 + dy).into();

        loop {
            match game.board.at_xy(current_check_pos.0, current_check_pos.1) {
                Err(_) => {
                    break;
                }
                Ok(opt) => match opt {
                    Some(piece) => {
                        println!("xy={:?} self={:?} other={:?}", current_check_pos, self, piece);
                        if piece.color == self.color {
                            break;
                        } else {
                            possible_moves.push(current_check_pos);
                            break;
                        }
                    }

                    None => possible_moves.push(current_check_pos),
                },
            }
            current_check_pos.0 += dx;
            current_check_pos.1 += dy;
        }
        return possible_moves;
    }
}
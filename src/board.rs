use std::{char::from_digit, ops::{Index, IndexMut}};

use crate::{piece::Piece, game::Game};

#[derive(Clone, Copy, Debug)]
pub struct Position (pub usize, pub usize);

impl From<(usize, usize)> for Position {
    fn from(value: (usize, usize)) -> Self {
        Position(value.0, value.1)
    }
}

impl From<usize> for Position {
    fn from(value: usize) -> Self {
        Position(value % 8, value / 8)
    }
}

impl From<Position> for String {
    fn from(value: Position) -> Self {
        let c1: u32 = 'a' as u32 + value.0 as u32;
        let mut str = String::new();
        str.push(char::from_u32(c1).unwrap_or('?'));
        str.push(from_digit(1 + value.1 as u32, 10).unwrap_or('?'));
        str
    }
}

pub struct ChessMove {
    pub from: Position,
    pub to: Position,
}

pub struct Board {
    board_arr: [Option<Piece>; 64],
}

impl Board {
    pub fn at_xy(&self, x: usize, y: usize) -> Result<&Option<Piece>, ()> {
        if x >= 8 || y >= 8 {
            return Err(());
        }
        self.board_arr.get(y * 8 + x).ok_or(())
    }

    pub fn at_xy_mut(&mut self, x: usize, y: usize) -> Result<&mut Option<Piece>, ()> {
        if x >= 8 || y >= 8 {
            return Err(());
        }
        self.board_arr.get_mut(y * 8 + x).ok_or(())
    }

    pub fn new() -> Self {
        Board {
            board_arr: [None; 64],
        }
    }
}

impl Index<Position> for Board {
    type Output = Option<Piece>;

    fn index(&self, index: Position) -> &Self::Output {
        self.at_xy(index.0, index.1).unwrap()
    }
}

impl IndexMut<Position> for Board {
    fn index_mut(&mut self, index: Position) -> &mut Self::Output {
        self.at_xy_mut(index.0, index.1).unwrap()
    }
}

impl Game {
    pub fn new(board: Board) -> Self {
        Game {board, en_passant_square: None, white_can_castle: true, black_can_castle: true, fifty_move_rule_count: 0}
    }
}